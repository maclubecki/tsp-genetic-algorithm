#include "ga_general.h"

void succession(const int S, int** population, int** children, long* fitnesses_p, long* fitnesses_c){
    //porownuje sie osobniki nowe ze starymi i do kolejnej populacji przechodza
    //lepsze z porownywanych (o mniejszym dopasowaniu)
    for(int i = 0; i < S; ++i){
        if(fitnesses_p < fitnesses_c)
            delete children[i];
        else{
            delete population[i];
            population[i] = children[i];
        }
    }
}
long geneticAlgorithm(const int iteration, const int S, const int N, const int q, const int crossOperator,
                      const int mutOperator, const float mutChance, const float optChance,
                      long* fitnesses_p, long* fitnesses_c, int** population, int** children, double** lengths)
{
    //zwraca najmniejsze dopasowanie z populacji

    //S - rozmiar populacji
    //N - rozmiar osobnika
    //q - rozmiar turnieju
    //crossOperator - wybrany operator krzyzowania
    //mutOperator - wybrany operator mutacji
    //mutChance - szansa na mutacje

    std::cout << "Iteration: " << iteration << "\n";
    //obliczanie dopasowania:
    for(int i = 0; i < S; ++i)
        fitnesses_p[i] = cycleLength(N,population[i],lengths);
    //improwizowane wzory dla samoadaptujacych sie parametrow:
    const float crossPercentage = 0.55 - ((float)iteration)/200;
    const int mut_k = 1 + N/(2+iteration/8);
    //tworzenie potomkow:
    createNextGeneration(S,N,q, crossOperator, crossPercentage, mutOperator, mutChance, mut_k, optChance,
                         fitnesses_p, population, children, lengths);
    for(int i = 0; i < S; ++i)
        fitnesses_c[i] = cycleLength(N,children[i],lengths);
    //sukcesja(postarzanie populacji):
    succession(S,population,children,fitnesses_p, fitnesses_c);

    return *std::min_element(fitnesses_p, fitnesses_p+S);
}
long loopGA(const int maxIterations, const int S, const int N, const int q, const int crossOperator,
                            const int mutOperator, const float mutChance, const float optChance,
                            long* fitnesses_p, long* fitnesses_c, int** population, int** children, double** lengths){
    int delta = 1;
    int iter = 0;
    long result_now = 0, result_old = 0;
    do
    {
        result_now = geneticAlgorithm(iter++,S,N,q,crossOperator,mutOperator,mutChance,optChance,
                                      fitnesses_p,fitnesses_c,population,children,lengths);
        delta = result_now - result_old;
        result_old = result_now;
    }
    while(delta != 0 || iter < maxIterations);

    std::cout << "\nFinal result: " << result_now << "\n";
    return result_now;
}
void createNextGeneration(const int S, const int N, const int q, const int crossOperator, const float crossPercentage,
                          const int mutOperator, const float mutChance, const int mut_k, const float optChance,
                          long* fitnesses, int** population, int** children, double** lengths)
{
    //crossPercentage - parametr do krzyzowania PMX i OX - wielkosc segmentu wzgledem calosci
    //mut_k - parametr do scramble mutation - ilosc elementow ktore sie losowo zamienia
    //pozostale opisane w geneticAlgorithm()

    for(int i = 0; i < S; ++i){
        //selekcja i krzyzowanie:
        int* p1 = population[tournamentSelection(S,q,fitnesses)];
        int* p2 = population[tournamentSelection(S,q,fitnesses)];
        switch(crossOperator){
            case 0:
                children[i] = crossPMX(N, crossPercentage, p1, p2);
                break;
            case 1:
                children[i] = crossOX(N, crossPercentage, p1, p2);
                break;
            default:
                children[i] = crossEX(N, p1, p2);
                break;
        }

        //mutacja:
        float r = (float)(rand()%1000)/1000;
        if(r < mutChance){

            switch(mutOperator){
            case 0:
                mutScramble(mut_k, N, children[i]);
                break;
            default:
                mutInversion(N,children[i]);
                break;
            }
        }

        //optymalizacja ala mutacja:
        r = (float)(rand()%1000)/1000;
        if(r < optChance){
            opt2(N,children[i],lengths);
        }

    }
}

long cycleLength(const int N, int* cycle, double** lengths){
    //takze funkcja dopasowania
    double l = 0;
    for(int i = 0; i < N-1; ++i)
        l+= lengths[cycle[i]][cycle[i+1]];
    l+= lengths[cycle[N-1]][cycle[0]];
    return (long)l;
}
int** initPopulation(const int S, const int N, double** lengths){
    //S - rozmiar populacji
    //N - rozmiar osobnika
    //2/3 populacji inicjujemy technika farthest insertion, reszta random
    std::cout << "Initializing population...\n";
    int ficount = 0;
    int optcount = 0;
    int** population;
    population = new int*[S];

    for(int i = 0; i < S; ++i){
        population[i] = new int[N];
        std::fill(population[i], population[i]+N, -1);
        if(i > S/3){
            hamiltonByFarthest(N,lengths,population[i]);
            ++ficount;
        }
        else hamiltonByRandom(N, population[i]);

        int r = rand() % 100;
        if(r > 50){
            opt2(N,population[i],lengths);
            ++optcount;
        }
    }
    std::cout << "Generated by farthest insertion: " << ficount << ", by random: " << S-ficount << ", optimized: " << optcount << ", total: " << S << "\n\n";
    return population;
}
int tournamentSelection(const int S, const int q, long* fitnesses){
    //funkcja ta zwraca indeks rodzica
    //losuje rodzicow bez zwracania, a potem wracaja oni do puli
    //tzn to jest po to, zeby ktorys osobnik nie pojawial sie 2+ razy i 'zapychal' miejsce dla innego osobnika
    //ale moze byc sytuacja ze 1 osobnik jest dzieckiem 1 rodzica
    std::vector<int> indexes(S);
    std::iota(indexes.begin(), indexes.end(), 0);
    std::vector<int> t;
    int r;
    for(int i = 0; i < q; ++i){
        r = indexes.at( rand()%indexes.size() );
        int d = std::distance(indexes.begin(), std::find(indexes.begin(), indexes.end(), r));
        indexes.erase(indexes.begin() + d);
        t.push_back(r);
    }
    //wektor dopasowan wybranych osobnikow
    std::vector<long> f;
    for(int i = 0; i < q; ++i)
        f.push_back( fitnesses[t.at(i)] );

    int best = std::distance(f.begin(), std::min_element(f.begin(), f.end()) );
    return t.at(best);
}
