OBJS =  main.cpp fileops.cpp insertion_heuristics.cpp tsp_operators.cpp ga_general.cpp 

CXX = g++

COMPILER_FLAGS = -std=c++11 -O2

all: $(OBJS)
	$(CXX) $(OBJS) $(COMPILER_FLAGS) -o ewo
