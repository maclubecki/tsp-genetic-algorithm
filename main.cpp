#include <iostream>
#include <cstdlib>
#include <chrono>
#include "fileops.h"
#include "tsp_operators.h"
#include "ga_general.h"

int main(int argc, char** argv) {
    if(argc < 9){
        std::cerr << "Not enough arguments (need 8).\n";
        return -1;
    }

    std::vector<std::vector<long double>*> v;
    for(int i = 0; i < 2; ++i) v.push_back(new std::vector<long double>());
    if(openFile(argv[1],v) != 0){
        std::cerr << "Couldn't open file.\n";
        return -1;
    }

    const int S = atoi(argv[2]);//rozmiar populacji
    const int N = v.at(0)->size();
    const int q = atoi(argv[3]);//rozmiar turnieju
    const int maxIterations = atoi(argv[4]);
    const int crossOperator = atoi(argv[5]);
    const int mutOperator = atoi(argv[6]);
    const float mutChance = atof(argv[7]);//0.2
    const float optChance = atof(argv[8]);//0.3;
    int repeats = 1;
    if(argc > 9)
        repeats = atoi(argv[9]);

    logArgs(argv[1],S,q,maxIterations,crossOperator,mutOperator,mutChance,optChance);

    double** lengths;
    lengths = new double*[N];
    for(int i = 0; i < N; ++i)
        lengths[i] = new double[N];
    fillLengths(N, lengths, v);
    int** children = new int*[S];
    long* fitnesses_p = new long[S];
    long* fitnesses_c = new long[S];
    srand(time(NULL));

    long* results = new long[10];
    double* times = new double[10];

    for(int i = 0; i < repeats; ++i){
        std::cout << "Repeat#" << i << "\n";
        std::clock_t start_t = std::clock();
        int** population = initPopulation(S, N, lengths);
        long result = loopGA(maxIterations,S,N,q,crossOperator,mutOperator,mutChance,optChance,
               fitnesses_p,fitnesses_c,population,children,lengths);

        double cputime = (std::clock() - start_t) / (double)CLOCKS_PER_SEC;
        results[i] = result;
        times[i] = cputime;
        std::cout << "CPU Time: " << cputime << "s\n";
        for(int i = 0; i < S; ++i){
            delete children[i];
            delete population[i];
        }
    }
    writeResult(results, times);

    return 0;
}
