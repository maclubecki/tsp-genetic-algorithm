#ifndef FILEOPS_H
#define FILEOPS_H
#include <cstdlib>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

void split(const std::string &s, char delim, std::vector<std::string> &elems);
void splitStr(const std::string &s, std::vector<std::vector<long double>*> &v);
int openFile(const std::string filename, std::vector<std::vector<long double>*> &v);

void writeLengths(int N, long** l);
void writeResult(long* fitness, double* cputime);
void fillLengths(const int N, double **l, std::vector<std::vector<long double>*> &v);

void logArgs(char* filename, const int S,const int q, const int maxIterations, const int crossOperator,
             const int mutOperator, const float mutChance, const float optChance);

#endif /* FILEOPS_H */

