#ifndef GA_GENERAL_H_INCLUDED
#define GA_GENERAL_H_INCLUDED

#include <cstdlib>
#include <vector>
#include <algorithm>
#include <iostream>
#include "insertion_heuristics.h"
#include "tsp_operators.h"

long cycleLength(const int N, int* cycle, double** lengths);
int** initPopulation(const int S, const int N, double** lengths);
int tournamentSelection(const int S, const int q, long* fitnesses);
void succession(const int S, int** population, int** children, long* fitnesses_p, long* fitnesses_c);
void createNextGeneration(const int S, const int N, const int q, const int crossOperator, const float crossPercentage,
                          const int mutOperator, const float mutChance, const int mut_k, const float optChance,
                          long* fitnesses, int** population, int** children, double** lengths);
long geneticAlgorithm(const int iteration, const int S, const int N, const int q, const int crossOperator,
                      const int mutOperator, const float mutChance, const float optChance,
                      long* fitnesses_p, long* fitnesses_c, int** population, int** children, double** lengths);
long loopGA(const int maxIterations, const int S, const int N, const int q, const int crossOperator,
                            const int mutOperator, const float mutChance, const float optChance,
                            long* fitnesses_p, long* fitnesses_c, int** population, int** children, double** lengths);


#endif // GA_GENERAL_H_INCLUDED
