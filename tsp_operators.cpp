#include "tsp_operators.h"

//Pomocnicze do EX:
void eraseFromEdgetable(const int N, int v, std::vector<int>* edge_table)
{
    //printf("Erasing: %d\n", v);
    for(int i = 0; i < N; ++i){
        auto f = std::find(edge_table[i].begin(), edge_table[i].end(), v);
        while(f != edge_table[i].end()){
            auto index = std::distance(edge_table[i].begin(), f);
            edge_table[i].erase(edge_table[i].begin() + index);
            f = std::find(edge_table[i].begin(), edge_table[i].end(), v);
        }
    }
}
int uniqueSize(std::vector<int> &v){
    //zliczanie unikalnych wartosci w wektorze; ta funkcja jest uzywana tylko dla v o max rozmiarze = 4
    //do operatora EX
    int s = 0;
    int last = -1;
    std::sort(v.begin(), v.end());
    for(int i = 0; i < v.size(); ++i){
        if(last == v.at(i))
            continue;
        last = v.at(i);
        ++s;
    }
    return s;
}
int getSmallestList(const int v, std::vector<int>* edge_table){
    //zwraca indeks najmniej licznej listy sposrod sasiadow wierzcholka v
    //lub -1 jesli wszystkie sa tego samego rozmiaru
    bool equal_sizes = true;
    int firstsize = -1;
    std::vector<int> sizes;
    for(int i = 0; i < edge_table[v].size(); ++i){
        //printf("E:%d,S:%d\n", edge_table[v].at(i), edge_table[edge_table[v].at(i)].size());
        sizes.push_back( uniqueSize(edge_table[edge_table[v].at(i)]) );
        if(i == 0) firstsize = sizes.back();
        if(firstsize != sizes.back()){
            equal_sizes = false;
        }
    }
    if(equal_sizes)
        return -1;

    auto m = std::min_element(sizes.begin(), sizes.end());
    return std::distance(sizes.begin(), m);
}
int searchDuplicate(const std::vector<int> &t){
    if(t.size() <= 1)
        return -1;
    for(int i = 0; i < t.size()-1; ++i){
        if(std::count(t.begin(), t.end(), t.at(i)) == 2)
            return i;
    }
    return -1;
}

void generateRange(const int N, const float max_size_percentage,int &p1, int &p2){
    int max_size = (int)(max_size_percentage*(float)N);

    while(p1==p2 || abs(p1-p2 - 1) > max_size){
        p1 = rand()%N;
        p2 = rand()%N;
    }
    if(p1 > p2) std::swap(p1,p2);
}

int* crossEX(const int N, int* parent1, int* parent2){
    //operator krzyzowania EX - edge crossover
    //edge table:
    std::vector<int> edge_table[N];
    for(int i = 0; i < N; ++i){//ta petla idzie po parentach
        //indeksy sasiadow
        int nv[2];
        nv[0] = i-1; nv[1] = i+1;
        if(nv[0] == -1)
            nv[0] = N-1;
        if(nv[1] == N)
            nv[1] = 0;

        for(int j = 0; j < 2; ++j){
            edge_table[parent1[i]].push_back(parent1[nv[j]]);
            edge_table[parent2[i]].push_back(parent2[nv[j]]);
        }
    }

    int* child = new int[N];
    std::fill(child, child+N, -1);
    std::vector<int> notfound(N);
    std::iota(notfound.begin(), notfound.end(), 0);
    int v = rand() % N;
    int cur = 0;

    //printf("Startv:%d\n",v);

    while(cur < N){
        //debugEdgetable(N,edge_table);
        eraseFromEdgetable(N, v, edge_table);
        child[cur++] = v;
        if(cur >= N)
            break;
        int v_ind = std::distance(notfound.begin(), std::find(notfound.begin(), notfound.end(), v));
        notfound.erase(notfound.begin() + v_ind);

        //ustalenie kolejnego elementu cyklu (v):
        //losowo jesli lista wierzcholkow pusta:
        if(edge_table[v].empty()){
            v = notfound.at(rand()%notfound.size());
            //printf("ByRandom:%d\n",v);
            continue;
        }
        int d = searchDuplicate(edge_table[v]);
        //najbardziej preferowany wybor: z wierzcholkiem w obu rodzicach
        if(d != -1){
            v = edge_table[v].at(d);
            //printf("By+:%d\n",v);
        }
        //najmniej liczna liste
        else{
            int t = getSmallestList(v, edge_table);
            if(t == -1)
                v = notfound.at(rand()%notfound.size());
            else
                v = edge_table[v].at(t);
            //printf("ByShortest:%d\n",v);
        }
    }
    return child;
}

int* crossPMX(const int N, const float max_size_percentage, int* parent1, int* parent2){
    //operator krzyzowania PMX - partially matched crossover
    //max_size_percentage - wielkosc segmentu wzgledem calosci
    int* child = new int[N];
    std::fill(child, child+N, -1);

    //wyznaczamy losowo odpowiadajace fragmenty z obu rodzicow
    //rozmiar tego fragmentu to conajmniej <size_percentage>% rozmiaru rodzica#1
    int p1 = 0, p2 = 0;
    generateRange(N,max_size_percentage,p1,p2);

    //uzupelnianie potomka:
    for(int i = p1; i <= p2; ++i){
        child[i] = parent1[i];
        //jesli w zakresie parenta1 nie ma elementu z parenta2:
        if(std::distance(parent1, std::find(parent1+p1,parent1+p2+1,parent2[i])) == p2+1){
            //f = index, gdzie znajduje sie element o wartosci parent1[i] w parent2
            int f = std::distance(parent2, std::find(parent2, parent2+N,parent1[i]));
            //przeskakiwanie dopoki element jest w zakresie [p1,p2]
            while(f >= p1 && f <= p2){
                //nowa szukana wartosc
                f = std::distance(parent2, std::find(parent2, parent2+N,parent1[f]));
            }
            child[f] = parent2[i];
        }
        else continue;
    }
    for(int i = 0; i < N; ++i)
        if(child[i] == -1) child[i] = parent2[i];

    /*
    //debug
    printf("Maxsize: %d, start: %d, stop: %d\n", (int)(max_size_percentage*(float)N), p1,p2);
    printf("Parent1:\n");
    debugCycleShort(parent1, N);
    printf("Parent2:\n");
    debugCycleShort(parent2, N);
    printf("Child:\n");
    debugCycleShort(child, N);
    */

    return child;
}

int* crossOX(const int N, const float max_size_percentage, int* parent1, int* parent2){
    //operator krzyzowania OX - order crossover
    //max_size_percentage - wielkosc segmentu wzgledem calosci
    int* child = new int[N];
    std::fill(child, child+N, -1);

    //wyznaczamy losowo odpowiadajace fragmenty z obu rodzicow
    //rozmiar tego fragmentu to conajmniej <size_percentage>% rozmiaru rodzica#1
    int p1 = 0, p2 = 0;
    generateRange(N,max_size_percentage,p1,p2);

    for(int i = p1; i <= p2; ++i)
        child[i] = parent1[i];
    //kopiowanie parenta2 do potomka bez duplikatow z parenta1
    int c_index = p2+1;
    for(int i = p2+1; i < N; ++i){
        if(std::distance(parent1,(std::find(parent1+p1,parent1+p2+1,parent2[i]))) == p2+1 ){
            child[c_index%N] = parent2[i];
            ++c_index;
        }
    }
    for(int i = 0; i < p2+1; ++i){
        if(std::distance(parent1,(std::find(parent1+p1,parent1+p2+1,parent2[i]))) == p2+1 ){
            child[c_index%N] = parent2[i];
            ++c_index;
        }
    }
    /*
    printf("Start: %d, stop: %d\n", p1,p2);
    printf("Parent1:\n");
    debugCycleShort(parent1, N);
    printf("Parent2:\n");
    debugCycleShort(parent2, N);
    printf("Child:\n");
    debugCycleShort(child, N);
    */
    return child;
}

void mutInversion(const int N, int *cycle){
    //lazy implementacja
    int p1 = 0,p2 = 0;
    generateRange(N,1.0,p1,p2);

    std::vector<int> temp;
    for(int i = p1; i <= p2; ++i)
        temp.push_back(cycle[i]);

    std::reverse(temp.begin(), temp.end());

    for(int i = p1, j = 0; i <= p2; ++i, ++j)
        cycle[i] = temp.at(j);
}
void mutScramble(const int k, const int N, int* cycle){
    //k - ilosc elementow ktore sie losowo zamienia
    if(k > N){
        std::cerr << "Niepoprawna wartosc parametru k dla scrambleMutation! k>N: " << k <<  " > " << N << "\n";
        return;
    }
    for(int i = 0; i < k; ++i){
        int x = rand()%N;
        int y = rand()%N;
        std::swap(cycle[x], cycle[y]);
    }
}

void opt2(const int N, int* cycle, double** lengths){
    //optymalizacja lokalna 2-opt, kod oparty o procedure TWOOPT z ksiazki Syslo
    //tymczasowa tablica do modyfikowania cyklu:
    int ptr[N];
    for(int i = 0; i < N-1; ++i)
        ptr[cycle[i]] = cycle[i+1];
    ptr[cycle[N-1]] = cycle[0];

    double delta, delta_t;
    int i1,i2,j1,j2;
    int s1,s2,t1,t2;

    //optymalizacja jest dokonywana dopoki przestanie sie poprawiac stan
    while(delta != 0){
        delta = 0;
        i1 = 0;
        for(int i = 0; i < N-2; ++i){
            int limit = N;
            if(i == 0) limit = N-1;
            i2 = ptr[i1];
            j1 = ptr[i2];
            for(int j = i+2; j < limit; ++j){
                j2 = ptr[j1];
                //sprawdzanie par krawedzi w poszukiwaniu dobrej wymiany
                delta_t = lengths[i1][i2]+lengths[j1][j2] - (lengths[i1][j1]+lengths[i2][j2]);
                //zapamietywanie "wspolrzednych" wymienionych krawedzi
                if(delta_t > delta){
                    delta = delta_t;
                    s1 = i1;
                    s2 = i2;
                    t1 = j1;
                    t2 = j2;
                }
                j1 = j2;
            }
            i1 = i2;
        }
        //jesli znaleziono korzystna wymiane krawedzi, tymczasowy cykl jest modyfikowany
        if(delta > 0){
            ptr[s1] = t1;
            int next = s2;
            int last = t2;
            int ahead;
            while(next != t2){
                ahead = ptr[next];
                ptr[next] = last;
                last = next;
                next = ahead;
            }
        }
    }
    //przeksztalcanie cyklu wejsciowego do postaci zmodyfikowanej
    int index = 1;
    for(int i = 0; i < N; ++i){
        cycle[i] = index;
        index = ptr[index];
    }
}
