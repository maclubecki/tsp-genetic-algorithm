#include "insertion_heuristics.h"

void hamiltonByRandom(const int N, int* cycle){
    std::vector<int> indexes(N);
    std::iota(indexes.begin(), indexes.end(),0);

    int cur = 0;
    while(!indexes.empty()){
        int r = rand()%indexes.size();
        cycle[cur++] = indexes.at(r);
        indexes.erase(indexes.begin()+ r);
    }
}
void alterDist(const int N, double* dist, const double* next_lengths){
    for(int i = 0; i < N; ++i)
        dist[i] = std::min(dist[i], next_lengths[i]);
}
void insertIntoCycle(const int N, const int f_index, int* cycle, double** lengths){
    std::vector<long> costs;
    std::vector<std::pair<int,int>> edges;
    int c1 = -1, c2 = -1;
    bool single_v = false;

    //znalezc krawedzie cyklu
    //obliczyc koszt dla nich
    for(int i = 0; i < N; ++i){
        if(cycle[i] != -1){
            c1 = i;
            c2 = cycle[i];
            //jesli nie ma krawedzi tylko pojedynczy wierzcholek
            if(c1 == c2){
                single_v = true;
                break;
            }
            //obliczenie i zapamietanie kosztu
            long c = lengths[c1][f_index] + lengths[f_index][c2] - lengths[c1][c2];
            costs.push_back(c);
            edges.push_back(std::pair<int,int>(c1,c2));
            c1 = c2;
            c2 = -1;
        }
    }
    if(single_v){
        cycle[c1] = f_index;
        cycle[f_index] = c1;
        return;
    }
    //wybrac najmniejszy koszt i dla odpowiadajacych wierzcholkow wstawic nowy wierzcholek
    auto min_cost = std::min_element(costs.begin(), costs.end());
    auto min_index = std::distance(costs.begin(), min_cost);
    cycle[edges.at(min_index).first] = f_index;
    cycle[f_index] = edges.at(min_index).second;
}
void hamiltonByFarthest(const int N, double** lengths, int* cycle){
    //wybiera losowo startowy wierzcholek
    //szuka najdalej polozonego wierzcholka OD CYKLU ktory jeszcze nie byl oznaczony
    int r = rand()%N;
    cycle[r] = r;
    //printf("First: %d\n", r);
    double dist[N];
    std::copy(lengths[r], lengths[r]+N, dist);

    for(int i = 0; i < N-1; ++i){//N-1 bo pierwszy krok to ustalenie losowego wierzcholka
        //okreslenie najdalej polozonego wierzcholka
        auto farthest = std::max_element(dist,dist+N);
        auto f_index = std::distance(dist, farthest);
        //wstawienie do cyklu
        insertIntoCycle(N, f_index, cycle, lengths);
        //apdejt odleglosci od cyklu
        alterDist(N, dist, lengths[f_index]);
    }
}
