#ifndef TSP_OPERATORS_H_INCLUDED
#define TSP_OPERATORS_H_INCLUDED
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <iostream>

//Pomocnicze do EX:
void eraseFromEdgetable(const int N, int v, std::vector<int>* edge_table);
int uniqueSize(std::vector<int> &v);
int getSmallestList(const int v, std::vector<int>* edge_table);
int searchDuplicate(const std::vector<int> &t);

//Pomocnicze do pozostalych:
void generateRange(const int N, const float max_size_percentage, int &p1, int &p2);

//Operatory krzyzowania:
int* crossEX(const int N, int* parent1, int* parent2);
int* crossPMX(const int N, const float max_size_percentage, int* parent1, int* parent2);
int* crossOX(const int N, const float max_size_percentage, int* parent1, int* parent2);

//Operatory mutacji:
void mutInversion(const int N, int *cycle);
void mutScramble(const int k, const int N, int* cycle);

//2-opt:
void opt2(const int N, int* cycle, double** lengths);

#endif // TSP_OPERATORS_H_INCLUDED
