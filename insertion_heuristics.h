#ifndef INSERTION_HEURISTICS_H_INCLUDED
#define INSERTION_HEURISTICS_H_INCLUDED
#include <vector>
#include <math.h>
#include <ctime>
#include <algorithm>

void hamiltonByRandom(const int N, int* cycle);
void alterDist(const int N, double* dist, const double* next_lengths);
void insertIntoCycle(const int N, const int f_index, int* cycle, double** lengths);
void hamiltonByFarthest(const int N, double** lengths, int* cycle);

#endif // INSERTION_HEURISTICS_H_INCLUDED
