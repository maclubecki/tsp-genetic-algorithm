#include "fileops.h"

void split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        if(!item.empty())
            elems.push_back(item);
    }
}

void splitStr(const std::string &s, std::vector<std::vector<long double>*> &v){
    std::vector<std::string> st;
    split(s,' ', st);
    char* end;
    for(int i = 1; i < st.size(); ++i){
        v.at(i-1)->push_back(strtold(st.at(i).c_str(), &end) );
    }
}
int openFile(const std::string filename, std::vector<std::vector<long double>*> &v){
    std::ifstream in;

    in.open(filename);
    if(!in.is_open())
        return -1;

    std::string l;
    while(!in.eof()){
        getline(in,l);
        if(atoi(l.c_str()) == 0)
            continue;
        splitStr(l,v);
    }
    in.close();
    return 0;
}

void writeLengths(int N, long** l){
    std::ofstream out;
    out.open("lengths.txt");
    for(int i = 0; i < N; ++i){
        for(int j = 0; j <= i; ++j)
            out << l[i][j] << " ";
        out << "\n";
    }
    out.close();
}
void writeResult(long* fitness, double* cputime){
    std::ofstream out;
    out.open("output.txt");
    for(int i = 0; i < 10; ++i)
        out << fitness[i] << " " << cputime[i] << std::endl;
    out.close();
}

void fillLengths(const int N, double **l, std::vector<std::vector<long double>*> &v){
    std::cout << "Filling distances matrix...\n";
    for(int i = 0; i < N; ++i)
        for(int j = 0; j <= i; ++j){
            if(i == j)
                l[i][j] = -1;
            else{
                long double x,y;
                x = (v.at(0)->at(i) - v.at(0)->at(j));
                y = (v.at(1)->at(i) - v.at(1)->at(j));
                l[i][j] = l[j][i] = sqrt(x*x + y*y);
            }
        }
    //v juz niepotrzebny
    for(int i = 0; i < 2; ++i)
        delete v.at(i);
    v.clear();
}

void logArgs(char* filename, const int S, const int q, const int maxIterations, const int crossOperator,
             const int mutOperator, const float mutChance, const float optChance)
{
    std::cout << "Travelling Salesman Problem\n\n";
    std::cout << "Filename: " << filename << "\nMax iterations: " << maxIterations << "\n";
    std::cout << "CrossOperator: ";
    switch(crossOperator){
    case 0:
        std::cout << "PMX, ";
        break;
    case 1:
        std::cout << "OX, ";
        break;
    default:
        std::cout << "EX, ";
        break;
    }
    std::cout << "Mutation operator: ";
    switch(mutOperator){
    case 0:
        std::cout << "Scramble";
        break;
    default:
        std::cout << "Inversion";
        break;
    }
    std::cout << "\nMutationChance: " << mutChance << ", Opt2Chance: " << optChance << "\n\n";

}

/*
void debugArray(double **lengths, int N){
    for(int i = 0; i < N; ++i){
        for(int j = 0; j < N; ++j){
            std::cerr << lengths[i][j] << " ";
        }
        std::cerr << "\n";
    }
}
void debugCycle(int* cycle, int N){
    for(int i = 0; i < N; ++i)
        printf("%d:%d\n",i,cycle[i]);
}
void debugCycleShort(int* cycle, int N){
    for(int i = 0; i < N; ++i)
        printf("%d:",cycle[i]);
    printf("\n");
}
void debugEdgetable(const int N, std::vector<int>* edge_table){
    printf("\nEdgetable: \n");
    for(int i = 0; i < N; ++i){
        printf("%d: ", i);
        for(int x: edge_table[i])
            printf("%d, ", x);
        printf("\n");
    }
}
*/
